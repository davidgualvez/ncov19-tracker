<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class NcovController extends Controller
{
    //
    private $host = 'https://api.covidph.org';

    public function totals(){
        $endpoint = '/api/totals';

        $client = new Client(); //GuzzleHttp\Client
        $req = $client->post($this->host.$endpoint);
        $resObj = json_decode( $req->getBody()->getContents() );

        $arr = [];

        foreach( $resObj as $key => $item ){

            // Deaths
            if( $item->id == 0){
                array_push($arr, [
                    'group'  => 'deaths',
                    'counts' => $item->value
                ]);
            }

            // Confirmed
            if( $item->id == 1){
                array_push($arr, [
                    'group' => 'confirmed',
                    'counts' => $item->value
                ]);
            }

            // Recovered
            if( $item->id == 2){
                array_push($arr, [
                    'group' => 'recovered',
                    'counts' => $item->value
                ]);
            }

            // Investigation
            if( $item->id == 3){
                array_push($arr, [
                    'group' => 'investigation',
                    'counts' => $item->value
                ]);
            }

        }

        return response()->json([
            'success'   => true,
            'message'   => 'Success',
            'data'      => $arr,
            // 'orig'      => $resObj
        ]);
    }

    public function locals(){
        $endpoint = '/api/moreDetails';

        $client = new Client(); //GuzzleHttp\Client
        $req = $client->post($this->host.$endpoint);
        $resObj = json_decode( $req->getBody()->getContents() );

        $arr = [];

        foreach( $resObj as $key => $item ){

            array_push($arr, [
                'local' => $item->code,
                'cases' => $item->current_confirmed,
                'death' => $item->current_dead,
                'recovered' => $item->current_recovered
            ]);

        }

        return response()->json([
            'success'   => true,
            'message'   => 'Success',
            'data'      => $arr,
            'orig'      => $resObj
        ]);
    }

    public function casesPerDay(){
        $endpoint = '/api/dateGraph2';

        $client = new Client(); //GuzzleHttp\Client
        $req = $client->post($this->host.$endpoint);
        $resObj = json_decode( $req->getBody()->getContents() );

        $arr = collect( $resObj ); 
        $sorted = $arr->sortKeysDesc();
        $now = Carbon::now();  
        $dateOfLastRecord = Carbon::parse($sorted->first()->date)->format('Y-m-d');
        $data = [
            'new'     => 0,
            'recovered' => 0,
            'deceased'  => 0
        ];
        
        if( $now->format('Y-m-d') == $dateOfLastRecord ){ 
            $sortedValues = $sorted->values();
            $data = [
                'new'     => $sorted->first()->confirmed - $sortedValues->get(1)->confirmed,
                'recovered' => $sorted->first()->recovered - $sortedValues->get(1)->recovered,
                'deceased'  => $sorted->first()->deaths - $sortedValues->get(1)->deaths
            ];
        }

        $perDay = [];
        foreach( $arr->values() as $key => $case){
            $date = Carbon::parse($case->date)->toFormattedDateString();
            if( $key == 0){
                array_push($perDay, [
                    'date'      => $date,
                    'case'      => $case->confirmed,
                    'deceased'  => $case->deaths,
                    'recovered' => $case->recovered
                ]); 
            }else{
                array_push($perDay, [
                    'date'      => $date,
                    'case'      => $case->confirmed - $arr->values()->get($key - 1)->confirmed,
                    'deceased'  => $case->deaths    - $arr->values()->get($key - 1)->deaths,
                    'recovered' => $case->recovered - $arr->values()->get($key - 1)->recovered
                ]); 
            }
        }

        return response()->json([
            'success'   => true,
            'message'   => 'Success',
            'data'      => [
                'today' => $data,
                'perDay'    => $perDay,
                'orig'      => $arr
            ]
            // 'orig'      => $resObj
        ]);
    }
}
 