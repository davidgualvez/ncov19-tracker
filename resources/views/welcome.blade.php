
<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>NCOV-19</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->

		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->

		<!--begin:: Vendor Plugins -->
		<link href="assets/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/quill/dist/quill.snow.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/dual-listbox/dist/dual-listbox.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/plugins/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/plugins/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/plugins/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Vendor Plugins -->
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--begin:: Vendor Plugins for custom pages -->
		<link href="assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/@fullcalendar/core/main.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/@fullcalendar/daygrid/main.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/@fullcalendar/list/main.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/@fullcalendar/timegrid/main.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/datatables.net-select-bs4/css/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/jqvmap/dist/jqvmap.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/uppy/dist/uppy.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body style="background-image: url(assets/media/demos/demo4/header.jpg); background-position: center top; background-size: 100% 200px;" class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
        
		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-container ">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
                                            NCOV19 - Tracker     
                                        </h3>
										<div class="kt-subheader__breadcrumbs">
											<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
                                                PHILIPPINES 
                                            </a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                                            <a href="javascript:;" class="kt-subheader__breadcrumbs-link">
                                                Novel Corona Virus Situation 
                                            </a>
										</div>
                                    </div> 
								</div>
							</div>

							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
 
                                {{-- Summaries --}}
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="kt-portlet kt-portlet--fit kt-portlet--head-overlay">
											<div class="kt-portlet__head kt-portlet__space-x">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title kt-font-light">
														Total Confirmed
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget27">
													<div class="kt-widget27__visual">
														<img src="assets/media/bg/bg-2.jpg" alt="" style="height: 150px!important;">
														<h3 class="kt-widget27__title">
                                                            <span id="t-confirmed">0</span>
														</h3>
													</div>
												</div>
											</div>
										</div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="kt-portlet kt-portlet--fit kt-portlet--head-overlay">
											<div class="kt-portlet__head kt-portlet__space-x">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title kt-font-light">
														Total Deaths
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget27">
													<div class="kt-widget27__visual">
														<img src="assets/media/bg/bg-2.jpg" alt="" style="height: 150px!important;">
														<h3 class="kt-widget27__title">
                                                            <!-- <span><span></span>0</span> -->
                                                            <span id="t-deaths">0</span>
														</h3>
													</div>
												</div>
											</div>
										</div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="kt-portlet kt-portlet--fit kt-portlet--head-overlay">
											<div class="kt-portlet__head kt-portlet__space-x">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title kt-font-light">
														Total Recovered
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget27">
													<div class="kt-widget27__visual">
														<img src="assets/media/bg/bg-2.jpg" alt="" style="height: 150px!important;">
														<h3 class="kt-widget27__title">
                                                            <span id="t-recovered">0</span>
														</h3>
													</div>
												</div>
											</div>
										</div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="kt-portlet kt-portlet--fit kt-portlet--head-overlay">
											<div class="kt-portlet__head kt-portlet__space-x">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title kt-font-light">
														Under Investigation
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget27">
													<div class="kt-widget27__visual">
														<img src="assets/media/bg/bg-2.jpg" alt="" style="height: 150px!important;">
														<h3 class="kt-widget27__title">
                                                            <span id="t-investigation">0</span>
														</h3>
													</div>
												</div>
											</div>
										</div>
                                    </div>
                                    
                                </div>
                                {{-- End of Summaries --}} 

                                <div class="row">

									<div class="col-md-6"> 

                                        <div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay ">
											<div class="kt-portlet__head kt-portlet__space-x">
												 
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget27">
													<div class="kt-widget27__visual">
														<img src="assets/media/bg/bg-4.jpg" alt="" style="height:150px!important;">
														<h3 class="kt-widget27__title" style="top:50%!important;">
															<span id="tc-today">+0</span>
														</h3>
														<div class="kt-widget27__btn">
															<a href="javascript:;" class="btn btn-pill btn-light btn-elevate btn-bold">
                                                                Todays Case
                                                            </a>
														</div>
													</div>
													<div class="kt-widget27__container kt-portlet__space-x">
                                                        <div class="kt-widget25"> 
                                                            <div class="kt-widget25__items">
                                                                <div class="kt-widget25__item text-center">
                                                                    <span id="tc-recovered" class="kt-widget25__number kt-font-success">
                                                                        0
                                                                    </span> 
                                                                    <span class="kt-widget25__desc">
                                                                        Recovered
                                                                    </span>
                                                                </div>
                                                                <div class="kt-widget25__item text-center">
                                                                    <span id="tc-deceased" class="kt-widget25__number kt-font-danger">
                                                                        0
                                                                    </span> 
                                                                    <span class="kt-widget25__desc">
                                                                        Deceased
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
													</div>
												</div>
											</div>
										</div> 

										<div class="kt-portlet">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Case Per Day
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
                                                <div class="kt-widget5 per-day-container"> 
                                                </div>
											</div>
										</div> 
                                    </div>

                                    <div class="col-md-6"> 
                                        <div class="kt-portlet">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">
													<h3 class="kt-portlet__head-title">
														Locals
													</h3>
												</div> 
											</div>
											<div class="kt-portlet__body">
                                                <div class="kt-widget5 locals-container"> 
                                                </div>
											</div>
										</div> 
                                    </div> 
                                </div>
                                 
							</div>

							<!-- end:: Content -->
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="background-image: url('assets/media/bg/bg-2.jpg');">
						<div class="kt-footer__top">
							<div class="kt-container ">
								<div class="row">
									<div class="col-lg-10">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">About COVID-19</h3>
											<div class="kt-footer__content">
                                            The disease causes respiratory illness (like the flu) with symptoms such as a cough, fever, and in more severe cases, difficulty breathing. You can protect yourself by washing your hands frequently, avoiding touching your face, and avoiding close contact (1 meter or 3 feet) with people who are unwell.
											</div>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="kt-footer__section">
											<h3 class="kt-footer__title">Reference</h3>
											<div class="kt-footer__content">
												<div class="kt-footer__nav">
													<div class="kt-footer__nav-section">
														<a href="javascript:;">Nefox Lab PH</a>
														<a href="javascript:;">Department of Health</a>
														<a href="javascript:;">World Health Organization</a>
													</div> 
												</div>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</div>
						<div class="kt-footer__bottom">
							<div class="kt-container ">
								<div class="kt-footer__wrapper">
									<!-- <div class="kt-footer__logo">
										<a class="kt-header__brand-logo" href="?page=index&amp;demo=demo2">
											<img alt="Logo" src="assets/media/logos/logo-4-sm.png" class="kt-header__brand-logo-sticky">
										</a>
										<div class="kt-footer__copyright">
											2019&nbsp;&copy;&nbsp;
											<a href="http://keenthemes.com/metronic" target="_blank">Keenthemes</a>
										</div>
									</div> -->
									<div class="kt-footer__menu">
										<a href="https://keepthingsup.com" target="_blank">David Gualvez</a>
									</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- end:: Footer -->

				</div>
			</div>
		</div>
		<!-- end:: Page -->
 
		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->

		<!--begin::Modal-->
		<div class="modal fade" id="locals-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<span>Under Construction</span>
					</div> 
				</div>
			</div>
		</div> 
		<!--end::Modal-->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#366cf3",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->

		<!--begin:: Vendor Plugins -->
		<script src="assets/plugins/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="assets/plugins/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="assets/plugins/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="assets/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="assets/plugins/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="assets/plugins/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="assets/plugins/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="assets/plugins/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="assets/plugins/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="assets/plugins/general/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="assets/plugins/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/dropzone.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/quill/dist/quill.js" type="text/javascript"></script>
		<script src="assets/plugins/general/@yaireo/tagify/dist/tagify.polyfills.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/@yaireo/tagify/dist/tagify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="assets/plugins/general/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/jquery-validation.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dual-listbox/dist/dual-listbox.js" type="text/javascript"></script>
		<script src="assets/plugins/general/raphael/raphael.js" type="text/javascript"></script>
		<script src="assets/plugins/general/morris.js/morris.js" type="text/javascript"></script>
		<script src="assets/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/plugins/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="assets/plugins/general/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="assets/plugins/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="assets/plugins/general/js/global/integration/plugins/sweetalert2.init.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="assets/plugins/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="assets/plugins/general/dompurify/dist/purify.js" type="text/javascript"></script>

		<!--end:: Vendor Plugins -->
		<script src="assets/js/scripts.bundle.js" type="text/javascript"></script>

		<!--begin:: Vendor Plugins for custom pages -->
		<script src="assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/core/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/daygrid/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/google-calendar/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/interaction/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/list/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/@fullcalendar/timegrid/main.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/dist/es5/jquery.flot.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.resize.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.categories.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.pie.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.stack.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.crosshair.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/flot/source/jquery.flot.axislabels.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/js/global/integration/plugins/datatables.init.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jszip/dist/jszip.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/pdfmake/build/pdfmake.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/pdfmake/build/vfs_fonts.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-buttons/js/buttons.print.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/jquery.vmap.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.world.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.russia.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.usa.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.germany.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.europe.js" type="text/javascript"></script>
		<script src="assets/plugins/custom/uppy/dist/uppy.min.js" type="text/javascript"></script>

		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script>
            $(document).ready(function(){
                console.log('hello friends');

                getTotalCases();
                getLocals();
                getTodaysCase();
            });

            function getTotalCases(){
                get('/totals',{}, function(res){
                    console.log(res);
                    if( res.success == false){
                        return;
                    }


                    $.each(res.data, function(k,v){
                        if(v.group == 'deaths'){
                            $('#t-deaths').text(v.counts);
                        }

                        if(v.group == 'confirmed'){
                            $('#t-confirmed').text(v.counts);
                        }

                        if(v.group == 'recovered'){
                            $('#t-recovered').text(v.counts);
                        }

                        if(v.group == 'investigation'){
                            $('#t-investigation').text(v.counts);
                        }
                    });


                });
            }

            function getLocals(){
                get('/locals', {}, function(res){
                    console.log(res);
                    if( res.success == false){
                        return;
                    }

                    var container = $('.locals-container');
                    container.empty();
                    $.each(res.data, function(k,v){
                        container.append(
                            `
							<a href="javascript:;" class="locals-detail">
								<div class="kt-widget5__item">
									<div class="kt-widget5__content">
										
										<div class="kt-widget5__section">
											<div href="javascript:;" class="kt-widget5__title">
												${v.local}
											</div>
											<p class="kt-widget5__desc">
												Location
											</p>
										</div>
										
									</div>
									<div class="kt-widget5__content">
										<div class="kt-widget5__stats">
											<span class="kt-widget5__number kt-font-brand">${v.cases}</span>
											<span class="">Cases</span>
										</div>
										<div class="kt-widget5__stats mr-3">
											<span class="kt-widget5__number kt-font-danger">${v.death}</span>
											<span class="">Deaths</span>
										</div>
										<div class="kt-widget5__stats">
											<span class="kt-widget5__number kt-font-success">${v.recovered}</span>
											<span class="">Recovered</span>
										</div>
									</div>
								</div>
							</a>
                            `
                        );
                    });


					$('.locals-detail').on('click', function(){
						console.log('test');
						$('#locals-detail').modal('toggle');
					});
                });

				
            }

            function getTodaysCase(){
                get('/cases-per-day', {}, function(res){
                    console.log(res);
                    if( res.success == false){
                        return;
                    }

                    $('#tc-today').text( '+'+res.data.today.new );
                    $('#tc-recovered').text( '+'+res.data.today.recovered );
                    $('#tc-deceased').text( '+'+res.data.today.deceased );


					var container = $('.per-day-container');
                    container.empty();
                    $.each(res.data.perDay, function(k,v){
                        container.append(
                            `
                            <div class="kt-widget5__item">
                                <div class="kt-widget5__content">
                                    
                                    <div class="kt-widget5__section">
                                        <a href="javascript:;" class="kt-widget5__title">
                                            ${v.date}
                                        </a>
                                        <p class="kt-widget5__desc">
                                            Date
                                        </p>
                                    </div>
                                    
                                </div>
                                <div class="kt-widget5__content">
                                    <div class="kt-widget5__stats">
                                        <span class="kt-widget5__number kt-font-brand">${v.case}</span>
                                        <span class="">New Case</span>
                                    </div>
                                    <div class="kt-widget5__stats mr-3">
                                        <span class="kt-widget5__number kt-font-danger">${v.deceased}</span>
                                        <span class="">Deaths</span>
                                    </div>
                                    <div class="kt-widget5__stats">
                                        <span class="kt-widget5__number kt-font-success">${v.recovered}</span>
                                        <span class="">Recovered</span>
                                    </div>
                                </div>
                            </div>
                            `
                        );
                    });

                });
            }

            //
            // Requests GET | POST
            //
            function post(url, payload, callback) {
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: payload,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {  
                        callback(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }

            function get(url, request, callback) {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    data: request,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {  
                        callback(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }

        </script>
		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>